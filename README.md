# Desktoppy

![Python version](https://img.shields.io/badge/python-3.11+-brightgreen)

Something cross-platformy for Python scripts that I was looking for myself.
Docs will follow soon!

## License
MIT
