import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

setup(
    name="desktoppy",
    version="0.1.0",
    description="Does something desktoppy",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/frankpublic/desktoppy",
    author="Frank Huurman",
    author_email="pypi-frank@protonmail.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
    packages=["desktoppy"],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "desktoppy=desktoppy.main:main",
        ]
    },
)
