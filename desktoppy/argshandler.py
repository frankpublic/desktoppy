import argparse
import platform


class ArgsHandler:

    def __init__(self) -> None:
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument("path", action="store", help="Path to your Python script")
        self.args = self.parser.parse_args()

        self._is_py_extension()
    
    def _is_py_extension(self) -> None:
        if not self.args.path.endswith(".py"):
            exit("The path argument needs to be a Python (.py) file")