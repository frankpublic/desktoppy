import platform
from .argshandler import ArgsHandler

platform_name = platform.system()

def main():
    """ The main program """
    args = ArgsHandler()
    print(f"Running on {platform_name}")
    print(args)


if __name__ == "__main__":
    main()